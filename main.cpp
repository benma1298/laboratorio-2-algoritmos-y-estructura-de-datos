
#include <iostream>
#include <string.h>
#include "Pila.h"

using namespace std;

// Funcion Opcion, despliega en la consola las opciones que el usuario puede elegir
void Opcion(){
    cout << "Opciones: " << endl;
    cout << "Agregar/Push  [1]" << endl;
    cout << "Remover/Pop   [2]" << endl;
    cout << "Ver Pila      [3]" << endl;
    cout << "Salir         [0]" << endl;
}

// Funcion main se encarga de recibir la dimension maxima de la pila (dim), la cual
// es definida por el usuario comenzando el programa
// Se usa la sentencia "switch" mientras que la variable opcion sea verdadera
// Los casos que puede tomar son las opciones de la funcion Opcion, usando la variable num
// la cual pasa de string a int usando la funcion "stoi"
//      case 1: "Agregar/Push": 
//                      Se ingresa el valor deseado en la variable dato,
//                      usando la funcion Push() de la clase Pila.cpp 
//      case 2: "Remover/Pop":
//                      Se llama a la funcion Pop() de la clase Pila.cpp para
//                      remover un elemento de la pila.
//      case 3: "Ver Pila":
//                      Se llama a la funcion Ver_pila() de la clase Pila.cpp para
//                      imprimir los datos agregados en la pila.
//      case 0: "Salir":
//                      Nos muestra el mensaje "El programa ha finalizado." y cambia el valor
//                      a la variable opcion (de true a false) para que se detenga el ciclo while.
int main(int argc, char* argv[]){
    string dim;
    string dato;
    string num;
    bool opcion = true;
    cout << "Cantidad Maxima de la Pila: ";
    getline(cin, dim);
    cout << endl;

    Pila pila = Pila(stoi(dim));
    while(opcion){ 
        Opcion();
        getline(cin, num);
        switch (stoi(num)){

            case 1:
            cout << "----------------------------------------------" << endl;
            cout << "Ingrese el valor que desea agregar a la Pila: ";
            getline(cin, dato);
            pila.Push(stoi(dato));
            cout << endl;
            break;

            case 2:
            pila.Pop();
            break;

            case 3:
            cout << "--------------------------" << endl;
            pila.Ver_pila();
            cout << "--------------------------" << endl;
            break;

            case 0:
            cout << "--------------------------" << endl;
            cout << "El programa ha finalizado." << endl;
            opcion = false;
            break;
        }
    }
    return 0;
}