
#include <iostream>
#include "Pila.h"

using namespace std;

// Constructor Clase Pila.cpp
Pila::Pila(int dim){
    this -> dim = dim;
    this -> TOPE = 0;
    this -> PILA = new int[dim];
    bool BAND;
}

// Metodos requeridos por el Laboratorio 2
void Pila::Pila_vacia(){
    if(TOPE == 0){
        this -> BAND = true;
    }else{
        this -> BAND = false;
    }
}
void Pila::Pila_llena(){
    if(TOPE == dim){
        this -> BAND = true;
    }else{
        this -> BAND = false;
    }
}
void Pila::Push(int dato){
    Pila_llena();
    if(this -> BAND){
        cout << "--------------------------" << endl;
        cout << "Desbordamiento, Pila llena" << endl;
    }else{
        this -> TOPE = TOPE + 1;
        this -> PILA[TOPE] = dato;
    }
}
void Pila::Pop(){
    Pila_vacia();
    if(this -> BAND){
        cout << "-----------------------------" << endl;
        cout << "Subdesbordamiento, Pila vacia" << endl;
    }else{
        this -> PILA[TOPE];
        this -> TOPE = TOPE - 1;
    }
}

// Metodo requerido en la opcion 3 para imprimir los datos de la Pila
void Pila::Ver_pila(){
    Pila_vacia();
    if(!this -> BAND){
        for(int i = this -> TOPE; i > 0; i--){
            cout << this -> PILA[i] << endl;
        }
    }else{
        cout << "----------------" << endl;
        cout << "NO HAY ELEMENTOS" << endl;
    }
}