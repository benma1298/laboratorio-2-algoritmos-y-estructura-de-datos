
Algortimos y Estructuras de Datos.
Laboratorio 2 Unidad 1.

Autor: Benjamin Martin.
Fecha: 12 de Septiembre del 2021.

Para ejecutar el Programa, ingresar en la terminal el comando "make" y en la siguiente linea de la terminal el comando "./main".
En este programa se implementan los algoritmos para manipular y mostrar una pila y los elementos que guarda.
Al comenzar el programa, se le pedira al usuario, mediante la consola, que ingrese la cantidad maxima de la pila.

Ademas de esto, se desplegaran las Opciones que puede elegir el usuario, las cuales son:
    "Agregar/Push  [1]"
    "Remover/Pop   [2]"
    "Ver Pila      [3]"
    "Salir         [0]"

El usuario debe elegir una de estas 4 opciones:
    -Opcion 1: Le pedira ingresar los datos que desea agregar a la pila.
    -Opcion 2: Removera el ultimo dato agregado a la pila.
    -Opcion 3: Imprimira todos los datos que posee la pila.
    -Opcion 0: Finalizara el programa en cuestion.