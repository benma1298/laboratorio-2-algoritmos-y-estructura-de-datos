
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
private:
    /* data */
    bool BAND;
    int TOPE = 0;
    int *PILA = 0;
    int dim = 0;
    
public:
    // Constructor
    Pila(int dim);

    // Metodos requeridos por el Laboratorio 2
    void Pila_vacia();
    void Pila_llena();
    void Push(int DATO);
    void Pop();

    // Metodo necesario en la opcion 3, para imprimir la pila
    void Ver_pila();
};

#endif